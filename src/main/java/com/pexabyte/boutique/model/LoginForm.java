package com.pexabyte.boutique.model;

/**
 * Created by omulakaluri on 5/15/2017.
 */
public class LoginForm {
    private String userName;
    private String password;

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
