package com.pexabyte.boutique.application;

import com.pexabyte.boutique.model.LoginForm;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Map;

@Controller
@RequestMapping("/boutique")
@EnableAutoConfiguration
public class BoutiqueApplication {
	@RequestMapping("/test")
	@ResponseBody
	String home() {
		return "Hello World!";
	}
	public static void main(String[] args) {
		SpringApplication.run(BoutiqueApplication.class, args);
	}


	@RequestMapping("/login")
	public String login(Map<String, Object> model) {
		model.put("message", "");
		return "login";
	}

	@RequestMapping("/success")
	public String success(Map<String, Object> model) {
		return "success";
	}

	@RequestMapping("/verify")
	public String login(Map<String, Object> model, @ModelAttribute LoginForm loginForm) {

		if(loginForm.getUserName().equals("pexabyte") && loginForm.getPassword().equals("pexabyte")) {
			model.put("username", loginForm.getUserName());
			return "success";
		}
		else {
			model.put("message", "Invalid Credentials. Please try again later");
			return "login";
		}

	}


}
